# Jike DailyCard API

即刻每日小卡片 API

## About

想做 jike calendar chrome extension 但苦于即刻 API 限制太严，抓包又不让，游客登录也没找到

虽然浏览器上只要登录了应该可以拦截 headers 拿 access token，但是好烦啊不想写

想着反正数据可以 cache 一天，找个免费后端方案多好

目标很简单

1. 计划任务
2. 免费托管

然而

- Heroku 免费 Dyno 会休眠 (pass)
- 提供 cron job 的普遍是触发 webhook，没有独立的 runtime (pass)
- 提供 runtime 的要求开 server，然而数据日更，static hosting 足够了 (pass)
- Github Pages 需要自己 cron commit (pass)

查了下 runtime 的话 CI 应该可以，本来还想借助 jsonblob 来托管，之后发现 Gitlab Pages 简直完美！

要 runtime 有 runtime，要 crontab 有 crontab，要 host 有 host，甚至报错还有邮件通知~

啊第一次用 CI，白嫖一时爽，一直白嫖一直爽，Gitlab 真香!

## Implementation

- **.gitlab-ci.yml** 是 CI 配置文件
- **fetch.js** 用来获得 access token，请求即刻 API 并把数据保存下来

> 感觉 python 的 image 比 nodejs 大所以就用 node 写了，能为 gitlab 省一点是一点  
> 好吧用 bash + curl 肯定也行的就是麻烦  
> Promise 没 catch 是因为反正报错了会有邮件提醒嘻嘻  

## Deploy

1. 在 Setting > CI / CD > Environment variables 设置变量 `x_jike_refresh_token`
2. 在 CI / CD > Schedules 里 New schedule 为每天 0 点触发
3. 收到 Pipeline failed 报警应该是 refresh token 过期了需要换一个？

## License

MIT