#!/usr/bin/env node

const request = (method, url, headers, body = null) => 
	new Promise((resolve, reject) => 
		(url.startsWith('https://') ? require('https').request : require('http').request)
		(Object.assign(require('url').parse(url), {method, headers}))
		.on('response', response => resolve(response))
		.on('error', error => reject(error))
		.end(body)
	)
	.then(response => 
		[201, 301, 302, 303, 307, 308].includes(response.statusCode) ? 
		request(method, require('url').parse(url).resolve(response.headers.location), headers, body) : 
		Object.assign(response, {json: () => new Promise((resolve, reject) => {
			let chunks = []
			response
			.on('data', chunk => chunks.push(chunk))
			.on('end', () => {try{resolve(JSON.parse(Buffer.concat(chunks)))}catch(error){reject(error)}})
			.on('error', error => reject(error))
		})})
	)


Promise.resolve()
.then(() => request('GET', 'https://app.jike.ruguoapp.com/1.0/app_auth_tokens.refresh', {
	'x-jike-refresh-token': process.env['x_jike_refresh_token']
}))
.then(response => response.statusCode === 200 ? response : Promise.reject(response.statusCode))
.then(response => response.json())
.then(data => request('GET', 'https://app.jike.ruguoapp.com/1.0/dailyCards/list', {
	'x-jike-access-token': data['x-jike-access-token']
}))
.then(response => response.statusCode === 200 ? response : Promise.reject(response.statusCode))
.then(response => response.pipe(require('fs').createWriteStream('./public/data.json')))